#!/bin/bash

# Installs Homebrew and some of the common dependencies needed/desired for software development

# Ask for the administrator password upfront
sudo -v

# Install .oh-my-zsh
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh

chsh -s /usr/local/bin/zsh

# Clone fonts
git clone https://github.com/powerline/fonts.git --depth=1
# Install the Powerline font
cd fonts
./install.sh
# clean-up a bit
cd ..
rm -rf fonts