#!/bin/bash

# Installs Homebrew and some of the common dependencies needed/desired for software development

# Ask for the administrator password upfront
sudo -v

# Xcode needs to be here first
xcode-select --install

# Install Homebrew (if it isn't already there)
if test ! $(which brew)
then
  echo "Installing Homebrew..."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# homebrew taps
brew tap homebrew/versions
brew tap homebrew/dupes
brew tap homebrew/php
brew tap caskroom/cask
brew tap caskroom/versions
brew tap homebrew/boneyard
brew tap caskroom/fonts


# Make sure we�re using the latest Homebrew
brew update

# Upgrade any already-installed formulae
brew upgrade --all

brew install ansible
brew install heroku
brew install nvm
brew install mongodb
brew install coreutils
brew install moreutils
brew install findutils
brew install ffmpeg
brew install gnu-sed --with-default-names
brew install grep --with-default-names
brew install brew-cask-completion
brew install grep
brew install openssh
brew install git
brew install git-extras
brew install wget
brew install node
brew install zsh
brew install zsh-completions
brew install python
brew install python3
brew install tree
brew install imagemagick --with-webp
brew install fortune
brew install tree
brew install tmux
brew install vim
brew install yarn
brew install nginx
brew install --without-apache --with-fpm --with-mysql php70
brew install mysql

# Remove outdated versions from the cellar
brew cleanup