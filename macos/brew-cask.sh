#!/bin/bash

# Installs Homebrew Cask apps

# Ask for the administrator password upfront
sudo -v

# Install Caskroom - We shoudln't need this since but let's keep it here anyway
## brew tap caskroom/cask
## brew install brew-cask
brew tap caskroom/versions

# Set the Appdir
export HOMEBREW_CASK_OPTS="--appdir=/Applications"

# Install packages
apps=(
    flux
    handbrake
    iterm2
    spectacle
    dash
    opera
    qbittorrent
    skype
    imagealpha
    imageoptim
    firefox
    vlc
    spotify
    teamviewer
    slack
    sublime-text
    transmit
    vagrant
    google-chrome
    google-chrome-canary
    google-drive-file-stream
    visual-studio-code
)

brew cask install "${apps[@]}"

# Quick Look Plugins (https://github.com/sindresorhus/quick-look-plugins)
brew cask install qlcolorcode qlstephen qlmarkdown quicklook-json qlprettypatch quicklook-csv betterzip qlimagesize webpquicklook suspicious-package
