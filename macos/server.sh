# Partly stolen from
# https://blog.frd.mn/install-nginx-php-fpm-mysql-and-phpmyadmin-on-os-x-mavericks-using-homebrew/

# Ask for the administrator password upfront
sudo -v

# Set-up Auto-start of NGINX
sudo cp -v /usr/local/opt/nginx/*.plist /Library/LaunchDaemons/
sudo chown root:wheel /Library/LaunchDaemons/homebrew.mxcl.nginx.plist

# Set-up Auto-start of php/MySQL
mkdir -p ~/Library/LaunchAgents
ln -sfv /usr/local/opt/php56/homebrew.mxcl.php56.plist ~/Library/LaunchAgents/
ln -sfv /usr/local/opt/mysql/*.plist ~/Library/LaunchAgents

# Create some dirs
mkdir -p /usr/local/etc/nginx/logs
mkdir -p /usr/local/etc/nginx/sites-available
mkdir -p /usr/local/etc/nginx/sites-enabled
mkdir -p /usr/local/etc/nginx/conf.d
sudo mkdir -p /var/www
sudo chown :staff /var/www
sudo chmod 775 /var/www

# Echo phpinfo(); into a test index.php file
echo "<?php phpinfo(); ?>" >> /var/www/index.php

# Move files to settings dirs
rm /usr/local/etc/nginx/nginx.conf
rsync -rtvu nginx/nginx.conf /usr/local/etc/nginx/
rsync -rtvu nginx/conf.d/* /usr/local/etc/nginx/conf.d/
rsync -rtvu nginx/sites-available/* /usr/local/etc/nginx/sites-available/

# Create system link for active servers
ln -sfv /usr/local/etc/nginx/sites-available/default /usr/local/etc/nginx/sites-enabled/default

# Load server services on launch
launchctl load -w ~/Library/LaunchAgents/homebrew.mxcl.php56.plist
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.mysql.plist
sudo launchctl load ~/Library/LaunchDaemons/homebrew.mxcl.nginx.plist

# Secure MySQL
mysql_secure_installation
